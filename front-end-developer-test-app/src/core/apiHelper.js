import persons from '../data/persons.json';

class ApiHelper {

  async getUsers(){
    return persons;
  }
}

export default new ApiHelper();