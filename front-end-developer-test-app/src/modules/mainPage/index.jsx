import React, { Component } from 'react';
import api from '../../core/apiHelper';
import Paper from './components/tableAndTextPaper/tableAndTextPaper';

class MainPage extends Component {
  constructor() {
    super();
    this.state = {
      users: null,
      dump: "",
    }
  }
  componentDidMount() {
    api.getUsers()
      .then(users => {
        this.setState({ users })
      })
      .catch(err => console.log(err))
  }

  deleteUser = (index,user) => {
    const { users } = this.state;
    users.splice(index, 1)
    this.logActions('DELETE',user);
    this.setState({ users })
  }

  handleCheck = (index,user) => {
    const { users } = this.state;
    users[index].employee = !users[index].employee;
    this.logActions('Employee',user);
    this.setState({ users })
  }
  addUser = (user) => {
    const { users } = this.state;
    users.push(user);
    this.logActions('ADD',user);
    this.setState({ users })
  }
  logActions = (type,user) => {
    this.setState({ dump: JSON.stringify(user) })
  }
  render() {
    const { dump } = this.state;
    return (
      <div>
        <Paper
          users={this.state.users}
          handleCheck={this.handleCheck}
          deleteUser={this.deleteUser}
          onInputChange={this.onInputChange}
          addUser={this.addUser}
          dump={dump} />
      </div>
    );
  }
}


export default MainPage;