import React from 'react';
import './addButton.scss';

const AddButton = (props) => (
    <div>
      <button {...props} className="addButton">Add</button>
    </div>
);

export default AddButton;