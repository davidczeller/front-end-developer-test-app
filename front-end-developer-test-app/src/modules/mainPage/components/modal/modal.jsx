import React from 'react';
import './modal.scss';

const Modal = ({ modalOpen, close, save,children }) => (
  <div className={modalOpen ? 'modal open' : 'modal'}>
    <div className='inner'>
      <div className="body">{children}</div>
      <div className="footer">
      <button type="footer"className="buttons" onClick={save}>OK</button>
      <button onClick={close} type="footer" className="buttons">Cancel</button>
      </div>      
    </div>
  </div>
);
export default Modal;