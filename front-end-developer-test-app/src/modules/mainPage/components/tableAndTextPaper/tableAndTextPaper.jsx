import React from 'react';
import Table from './table/table';
import './tableAndTextPaper.scss'


const Paper = (props) => (
  <div className="paper">
      <Table {...props}/>
  </div>
);

export default Paper;