import React from 'react';
import './textArea.scss';

const TextArea = (props) => (
  <div>
    <label htmlFor="textArea">Data dump</label>
     <textarea id="textArea" {...props}/>
  </div>
);

export default TextArea;