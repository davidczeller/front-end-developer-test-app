import React, { Component } from 'react';
import './table.scss';
import TextArea from '../textArea/textArea';
import CheckBox from '../checkbox/checkbox';
import Add from '../../addButton/addButton';
import Modal from '../../modal/modal';

class Table extends Component {
  constructor() {
    super();
    this.state = {
      modalOpen: false,
      user: {
        name: '',
        nick: '',
        job: '',
        age: '',
        employee: false,
      }
    }
  }

  renderUsers = () => {
    const { users } = this.props;
    if (!users) return;
    return users.map((user, index) => (
      <tr key={index + user.name}>
        <td>{user.name}<br /> {user.job}</td>
        <td>{user.age}</td>
        <td>{user.nick}</td>
        <td>
          <CheckBox checked={user.employee} onChange={() => this.props.handleCheck(index,user)} index={index}/>
        </td>
        <td><button onClick={() => this.props.deleteUser(index,user)} className="deleteButton" >Delete</button></td>
      </tr>
    ))
  };

  closeModal = () => { this.setState({ modalOpen: false }) }
  onSave = () => {
    const { user } = this.state;
    const init = {
      name: '',
      nick: '',
      job: '',
      age: '',
      employee: false,
    }
    this.props.addUser(user);
    this.setState({ modalOpen: false, user: init })
  }

  onInputChange = ({ name, value }) => {
    const { user } = this.state;
    if (name === "employee") {
      user[name] = !user[name];
    } else {
      user[name] = value;
    }
    this.setState({ user })
  }

  render() {
    const { modalOpen, user } = this.state;
    const { dump } = this.props;
    return (
      <div>
        <Add onClick={() => this.setState({ modalOpen: true })} />
        <Modal modalOpen={modalOpen} close={this.closeModal} save={this.onSave} >
          <div className="form">
            <label htmlFor="name">Name</label>
            <input type="text" name="name" id="name" value={user.name} onChange={({ target }) => this.onInputChange(target)} />
            <label htmlFor="job">Job title</label>
            <input type="text" name="job" id="job" value={user.job} onChange={({ target }) => this.onInputChange(target)} />
            <label htmlFor="age">Age</label>
            <input type="number" name="age" id="age" value={user.age} onChange={({ target }) => this.onInputChange(target)} />
            <label htmlFor="nick">Nickname</label>
            <input type="text" name="nick" id="nick" value={user.nick} onChange={({ target }) => this.onInputChange(target)} />
            <label htmlFor="employee">Employee</label>
            <input type="checkbox" name="employee" id="employee" value={user.employee} checked={user.employee} onChange={({ target }) => this.onInputChange(target)} />
          </div>
        </Modal>
        <table>
          <thead>
            <tr>
              <th><p>Name</p><p>(job title)</p></th>
              <th>Age</th>
              <th>Nickname</th>
              <th>Emloyee</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.renderUsers()}
          </tbody>
        </table>
        <TextArea value={dump} />
      </div>
    )
  }
};

export default Table;