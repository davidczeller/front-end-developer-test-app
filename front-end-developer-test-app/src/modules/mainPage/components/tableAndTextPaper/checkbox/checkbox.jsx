import React from "react";

const CheckBox = props => (
  <div>
    <label  className={props.value ? "s-label" : "s-defaultLabel"} htmlFor={props.index}>
      {props.title}
    </label>
    <input type="checkbox"  id={props.index} {...props} />
  </div>
);

export default CheckBox;
